package com.capgemini.cags.api.batch;

import java.util.Base64;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.support.ListItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Main configuration class.
 * 
 * @author vquartar
 *
 */
@Configuration
@ComponentScan(basePackages = "com.capgemini.cags.batch")
@EnableBatchProcessing
public class BatchConfiguration {
	
	private static final Logger log = LoggerFactory.getLogger(BatchConfiguration.class);
	
	@Autowired
	private com.capgemini.cags.api.batch.Configuration config;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    
    @Autowired
    public RestTemplate restTemplate;
    
    @Autowired
    private AccessTokenResponse accessToken;
    
    @Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
    
    // tag::readerwriterprocessor[]
    @Bean
    public FlatFileItemReader<SOAPService> reader() {
        return new FlatFileItemReaderBuilder<SOAPService>()
            .name("serviceItemReader")
            .resource(new FileSystemResource(config.getInputFile()))
            .delimited()
            .names(config.getInputFields())
            .fieldSetMapper(new BeanWrapperFieldSetMapper<SOAPService>() {{
                setTargetType(SOAPService.class);
            }})
            .build();
    }
    

    @Bean
    public SOAPServiceProcessor processor() {
    	final String baseUrl = "https://"+config.getApimHost()+":"+config.getApimPort();
    	final String tokenBaseUrl = "https://"+config.getApimHost()+":"+config.getApimTokenPort();
    	
    	try {
	    	//register client
	    	String dynamicClientRegistrationEndpoint = baseUrl+"/client-registration/v0.11/register";
			ClientRegistrationRequest request = new ClientRegistrationRequest();
			request.setClientName("RestTemplate");
			request.setOwner("admin");
			request.setGrantType("password refresh_token");
			
			String plainCreds = config.getUsername()+":"+config.getPassword();
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
	
			HttpHeaders clientRegistrationHeaders = new HttpHeaders();
			clientRegistrationHeaders.add("Authorization", "Basic " + base64Creds);
			
			HttpEntity<ClientRegistrationRequest> clientRegistrationRequestHttpEntity = new HttpEntity<>(request, clientRegistrationHeaders);
			ClientRegistrationResponse clientRegistration  = restTemplate.postForObject(dynamicClientRegistrationEndpoint, clientRegistrationRequestHttpEntity, ClientRegistrationResponse.class);
			log.info("ClientName={}, got credentials: {} {}", clientRegistration.getClientName(), clientRegistration.getClientId(), clientRegistration.getClientSecret());
			
			
			//get AT
			String tokenEndpoint = tokenBaseUrl+"/token";
			plainCreds = clientRegistration.getClientId()+":"+clientRegistration.getClientSecret();
			plainCredsBytes = plainCreds.getBytes();
			base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
			base64Creds = new String(base64CredsBytes);
			HttpHeaders tokenHeaders = new HttpHeaders();
			tokenHeaders.add("Authorization", "Basic " + base64Creds);
			tokenHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	
			String tokenRequestBody = "grant_type=password&username=admin&password=admin&scope=apim:api_view%20apim:api_create%20apim:api_publish";
			HttpEntity <String> tokenRequestHttpEntity = new HttpEntity <String> (tokenRequestBody, tokenHeaders);
			AccessTokenResponse accessTokenResponse = restTemplate.postForObject(tokenEndpoint, tokenRequestHttpEntity, AccessTokenResponse.class);
			accessToken.setAccess_token(accessTokenResponse.getAccess_token());
			accessToken.setRefresh_token(accessTokenResponse.getRefresh_token());
			accessToken.setExpires_in(accessTokenResponse.getExpires_in());
			accessToken.setScope(accessTokenResponse.getScope());
			accessToken.setToken_type(accessTokenResponse.getToken_type());
			log.info("got ACCESS TOKEN: {}, scopes: {}, expires in {} seconds", accessToken.getAccess_token(), accessToken.getScope(), accessToken.getExpires_in());
    	} catch (RestClientException e) {
    		e.printStackTrace();
    		log.error("Errore di comunicazione con WSO2 API Manager: {}", e.getMessage());
    		System.exit(1);
    	}
        return new SOAPServiceProcessor();
    }
    

    @Bean
    public NoOpItemWriter<SOAPService> writer(DataSource dataSource) {
        return new NoOpItemWriter<SOAPService>();
    }
    // end::readerwriterprocessor[]
	
    @Bean
    public TaskExecutor taskExecutor(){
        SimpleAsyncTaskExecutor asyncTaskExecutor=new SimpleAsyncTaskExecutor("spring_batch");
        asyncTaskExecutor.setConcurrencyLimit(config.getThreads());
        return asyncTaskExecutor;
    }

    // tag::jobstep[]
    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .flow(step1)
            .end()
            .build();
    }

    @Bean
    public Step step1(ListItemWriter<SOAPService> writer) {
        return stepBuilderFactory.get("step1")
            .<SOAPService, SOAPService> chunk(config.getChunkSize())
            .reader(reader())
            .processor(processor())
            .writer(writer)
            .taskExecutor(taskExecutor())
            .build();
    }
    // end::jobstep[]
}