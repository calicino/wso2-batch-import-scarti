package com.capgemini.cags.api.batch;

import java.util.List;

import org.springframework.batch.item.support.ListItemWriter;

/**
 * No-operation ItemWriter. It's not required to do anything at the end of a single SOAP service processing.
 * @author vquartar
 *
 * @param <T>
 */
public class NoOpItemWriter<T> extends ListItemWriter<T> {
	
	@Override
	public void write(List<? extends T> arg0) throws Exception {
		//does nothing.
	}

}
