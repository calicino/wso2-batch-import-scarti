package com.capgemini.cags.api.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application main class.
 * 
 * @author vquartar
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
public class Application {
	
	public static void main(String args[]) {
		SpringApplication.run(Application.class, args);
	}

}