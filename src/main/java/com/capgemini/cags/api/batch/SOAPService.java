package com.capgemini.cags.api.batch;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Models the WSO2 Publihser API payload, used to create APIs.
 * @author vquartar
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SOAPService {
	
	private static final String ENDPOINT_CONFIG_TEMPLATE = "{\"production_endpoints\":{\"url\":\"@endpoint@\",\"endpoint_type\":\"address\",\"template_not_supported\":false},\"sandbox_endpoints\":{\"url\":\"@endpoint@\",\"endpoint_type\":\"address\",\"template_not_supported\":false},\"endpoint_type\":\"address\"}";
	private static final String API_DEFINITION_TEMPLATE = "{\"paths\":{\"/*\":{\"post\":{\"parameters\":[{\"schema\":{\"type\":\"string\"},\"description\":\"SOAP request.\",\"name\":\"SOAP Request\",\"required\":true,\"in\":\"body\"},{\"description\":\"SOAPAction header for soap 1.1\",\"name\":\"SOAPAction\",\"type\":\"string\",\"required\":false,\"in\":\"header\"}],\"responses\":{\"200\":{\"description\":\"OK\"}},\"x-auth-type\":\"Application & Application User\",\"x-throttling-tier\":\"Unlimited\"}}},\"swagger\":\"2.0\",\"consumes\":[\"text/xml\",\"application/soap+xml\"],\"produces\":[\"text/xml\",\"application/soap+xml\"],\"info\":{\"title\":\"@name@\",\"version\":\"1.0\"}}";
	
	private String id;
	private String name;
	private String description;
	private String context;
	private String version = "1.0";
	private String provider = "admin";
	private String apiDefinition;
	private String status;
	private String wsdlUri;
	private String responseCaching  ="Disabled";
	private int cacheTimeout = 300;
	private String destinationStatsEnabled;
	private boolean isDefaultVersion = true;
	private String type = "HTTP";
	private String[] transport = {"http", "https"};
	private String[] tags = {};
	private String[] tiers = {"Unlimited"};
	@JsonIgnore
	private String apiLevelPolicy;
	private Map<String, Integer> maxTps;
	private String thumbnailUri;
	private String visibility = "PUBLIC";
	private String [] visibleRoles = {};
	private String accessControl = "NONE";
	private String [] accessControlRoles = {};
	private String [] visibleTenants = {};
	private String endpointConfig;
	private Map<String, String> endpointSecurity;
	private String gatewayEnvironments = "Production and Sandbox";
	private String[] sequences = {};
	private String subscriptionAvailability;
	private String[] subscriptionAvailableTenants = {};
	private Map<String, String> businessInformation;
	private Map<String, Object> corsConfiguration;
	@JsonIgnore
	private String endpoint;
	
	public SOAPService() {
		businessInformation = new HashMap<>();
		businessInformation.put("businessOwner", "Credit Agricole Group Solutions");
		businessInformation.put("technicalOwner", "Credit Agricole Group Solutions");
		corsConfiguration = new HashMap<>();
		corsConfiguration.put("accessControlAllowOrigins", new String[]{"*"});
		corsConfiguration.put("accessControlAllowCredentials", false);
		corsConfiguration.put("corsConfigurationEnabled", false);
		corsConfiguration.put("accessControlAllowHeaders", new String[]{"authorization", "Access-Control-Allow-Origin", "Content-Type", "SOAPAction"});
		corsConfiguration.put("accessControlAllowMethods", new String[]{"GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS"});
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
		this.apiDefinition = API_DEFINITION_TEMPLATE.replaceAll("@name@", name);
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getApiDefinition() {
		return apiDefinition;
	}
	public void setApiDefinition(String apiDefinition) {
		this.apiDefinition = apiDefinition;
	}
	public String getWsdlUri() {
		return wsdlUri;
	}
	public void setWsdlUri(String wsdlUri) {
		this.wsdlUri = wsdlUri;
	}
	public String getResponseCaching() {
		return responseCaching;
	}
	public void setResponseCaching(String responseCaching) {
		this.responseCaching = responseCaching;
	}
	public int getCacheTimeout() {
		return cacheTimeout;
	}
	public void setCacheTimeout(int cacheTimeout) {
		this.cacheTimeout = cacheTimeout;
	}
	public String getDestinationStatsEnabled() {
		return destinationStatsEnabled;
	}
	public void setDestinationStatsEnabled(String destinationStatsEnabled) {
		this.destinationStatsEnabled = destinationStatsEnabled;
	}
	public boolean getIsDefaultVersion() {
		return isDefaultVersion;
	}
	public void setIsDefaultVersion(boolean isDefaultVersion) {
		this.isDefaultVersion = isDefaultVersion;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String[] getTransport() {
		return transport;
	}
	public void setTransport(String[] transport) {
		this.transport = transport;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public String[] getTiers() {
		return tiers;
	}
	public void setTiers(String[] tiers) {
		this.tiers = tiers;
	}
	public Map<String, Integer> getMaxTps() {
		return maxTps;
	}
	public void setMaxTps(Map<String, Integer> maxTps) {
		this.maxTps = maxTps;
	}
	public String getThumbnailUri() {
		return thumbnailUri;
	}
	public void setThumbnailUri(String thumbnailUri) {
		this.thumbnailUri = thumbnailUri;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String[] getVisibleRoles() {
		return visibleRoles;
	}
	public void setVisibleRoles(String[] visibleRoles) {
		this.visibleRoles = visibleRoles;
	}
	public String getEndpointConfig() {
		return endpointConfig;
	}
	public void setEndpointConfig(String endpointConfig) {
		this.endpointConfig = endpointConfig;
	}
	public Map<String, String> getEndpointSecurity() {
		return endpointSecurity;
	}
	public void setEndpointSecurity(Map<String, String> endpointSecurity) {
		this.endpointSecurity = endpointSecurity;
	}
	public String getGatewayEnvironments() {
		return gatewayEnvironments;
	}
	public void setGatewayEnvironments(String gatewayEnvironments) {
		this.gatewayEnvironments = gatewayEnvironments;
	}
	public String[] getSequences() {
		return sequences;
	}
	public void setSequences(String[] sequences) {
		this.sequences = sequences;
	}
	public String getSubscriptionAvailability() {
		return subscriptionAvailability;
	}
	public void setSubscriptionAvailability(String subscriptionAvailability) {
		this.subscriptionAvailability = subscriptionAvailability;
	}
	public String[] getSubscriptionAvailableTenants() {
		return subscriptionAvailableTenants;
	}
	public void setSubscriptionAvailableTenants(String[] subscriptionAvailableTenants) {
		this.subscriptionAvailableTenants = subscriptionAvailableTenants;
	}
	public Map<String, String> getBusinessInformation() {
		return businessInformation;
	}
	public void setBusinessInformation(Map<String, String> businessInformation) {
		this.businessInformation = businessInformation;
	}
//	public CorsConfiguration getCorsConfiguration() {
//		return corsConfiguration;
//	}
//	public void setCorsConfiguration(CorsConfiguration corsConfiguration) {
//		this.corsConfiguration = corsConfiguration;
//	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Map<String, Object> getCorsConfiguration() {
		return corsConfiguration;
	}
	public void setCorsConfiguration(Map<String, Object> corsConfiguration) {
		this.corsConfiguration = corsConfiguration;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
		this.endpointConfig = ENDPOINT_CONFIG_TEMPLATE.replaceAll("@endpoint@", endpoint);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@JsonIgnore
	public String getApiLevelPolicy() {
		return apiLevelPolicy;
	}

	public void setApiLevelPolicy(String apiLevelPolicy) {
		this.apiLevelPolicy = apiLevelPolicy;
	}
	@JsonIgnore
	public String getAccessControl() {
		return accessControl;
	}

	public void setAccessControl(String accessControl) {
		this.accessControl = accessControl;
	}
	@JsonIgnore
	public String[] getAccessControlRoles() {
		return accessControlRoles;
	}

	public void setAccessControlRoles(String[] accessControlRoles) {
		this.accessControlRoles = accessControlRoles;
	}

	public String[] getVisibleTenants() {
		return visibleTenants;
	}

	public void setVisibleTenants(String[] visibleTenants) {
		this.visibleTenants = visibleTenants;
	}

	public void setDefaultVersion(boolean isDefaultVersion) {
		this.isDefaultVersion = isDefaultVersion;
	}
	
}
