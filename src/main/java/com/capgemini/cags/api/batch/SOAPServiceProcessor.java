package com.capgemini.cags.api.batch;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
/**
 * Process a single SOAP service by publishing it to the gateway.
 * @author vquartar
 *
 */
public class SOAPServiceProcessor implements ItemProcessor<SOAPService, SOAPService> {
	
	private static final Logger log = LoggerFactory.getLogger(SOAPServiceProcessor.class);
	
	@Autowired
	private Configuration config;
	
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private AccessTokenResponse accessToken;

	@Override
	public SOAPService process(SOAPService service) throws Exception {
		final String baseUrl = "https://"+config.getApimHost()+":"+config.getApimPort();
		log.debug("baseUrl={}", baseUrl);
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			HttpHeaders tokenHeaders = new HttpHeaders();
			tokenHeaders.add("Authorization", accessToken.getToken_type() + " " + accessToken.getAccess_token());
			tokenHeaders.setContentType(MediaType.APPLICATION_JSON);
			
			//drop if exists
			String searchAPIuri = baseUrl+"/api/am/publisher/v0.11/apis?query=name:"+service.getName();
			HttpEntity <String> searchAPIRequestHttpEntity = new HttpEntity <String> (tokenHeaders);
			ResponseEntity<String> searchAPIResponse = restTemplate.exchange(searchAPIuri, HttpMethod.GET, searchAPIRequestHttpEntity, String.class);
			JSONObject jsonObj = new JSONObject(searchAPIResponse.getBody());
			int count = (int) jsonObj.get("count");
			if (count==1) {
				JSONArray apiList = (JSONArray) jsonObj.get("list");
				JSONObject api = (JSONObject) apiList.get(0);
				String apiId = api.getString("id");
				log.info("deleting existing API with id: {}", apiId);
				String deleteAPIUrl = baseUrl+"/api/am/publisher/v0.11/apis/" + apiId;
				//restTemplate.delete(deleteAPIUrl);
				restTemplate.exchange(deleteAPIUrl, HttpMethod.DELETE, searchAPIRequestHttpEntity, String.class);
			}
	
			//create
			String createAPIuri = baseUrl+"/api/am/publisher/v0.11/apis";
			String [] a = {config.getTag()};
			if(!a[0].equals("null")) {
				a[0]=a[0]+"-group";
				service.setTags(a);
			}
			String createAPIRequestBody = mapper.writeValueAsString(service);
			log.info("sending body: {}", createAPIRequestBody);
			HttpEntity <String> createAPIRequestHttpEntity = new HttpEntity <String> (createAPIRequestBody, tokenHeaders);
			log.info("info---------------");
			SOAPService APIcreated = restTemplate.postForObject(createAPIuri, createAPIRequestHttpEntity, SOAPService.class);
			log.info("info2-------------");
			log.info("created API: {}", APIcreated.getId());
			
			
			log.info("sleep");
			TimeUnit.SECONDS.sleep(config.getSleep());			
	
			//publish
			String publishAPIUri = baseUrl+"/api/am/publisher/v0.11/apis/change-lifecycle?apiId="+APIcreated.getId()+"&action=Publish";
			HttpEntity <String> publishAPIRequestHttpEntity = new HttpEntity <String> (tokenHeaders);
			restTemplate.exchange(publishAPIUri, HttpMethod.POST, publishAPIRequestHttpEntity, String.class);
			
			log.info("API Published");

		} catch (RestClientException e) {
			log.error("Errore di comunicazione con WSO2 API Manager: {}", e.getMessage());
			log.info(service.getName());
			//log.info(service.getContext());
			//log.info(service.getDescription());
			//log.info(service.getEndpoint());
			try {
			    File file = new File(config.getOutputFile());
			    FileWriter fw = new FileWriter(file,true);
			    //System.out.println("SCRIVO API SCOPPIATA");
			    fw.write("\""+service.getName()+"\","+"\""+service.getDescription()+"\","+"\""+service.getContext()+"\","+service.getEndpoint()+"\"\n");
			    fw.flush();
			    fw.close();
			    } catch (IOException e1) {
			    e1.printStackTrace();
			    }
			    e.printStackTrace();
			    //System.exit(1);
    	}
		
		return service;
	}

}
